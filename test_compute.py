#!/usr/bin/python

__author__= " VIVEK SHARMA "

import os
import sys
import re
import string
import time
import ConfigParser
import datetime
from subprocess import Popen, PIPE, STDOUT
import subprocess
import unittest
import ConfigParser
from modulevpcids import *
from modulecomputeids import *
from moduleiamids import *


class ComputeTest(unittest.TestCase):

    configParser = ConfigParser.RawConfigParser()
    configFilePath = r'cliconfig'
    configParser.read(configFilePath)
    keyname = configParser.get('COMPUTE', 'keyname')
    publicKey = configParser.get('COMPUTE', 'publicKey')
    vpccidr = configParser.get('COMPUTE', 'vpccidr')
    subnetcidr = configParser.get('COMPUTE', 'subnetcidr')

    def test_A_create_key_pair_compute(self):

        cmd111 = "jcs compute create-key-pair --key-name "+self.keyname
	print cmd111
        p111 = Popen(cmd111, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        command111out = p111.stdout.read()
        print command111out
 
    def test_B_describe_key_pair_compute(self):

        cmd112 = "jcs compute describe-key-pairs"
        print cmd112
        p112 = Popen(cmd112, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        keypairjson = p112.stdout.read()
        print keypairjson

    def test_C_delete_key_pair_compute(self):
    
        cmd113 = "jcs compute delete-key-pair --key-name "+self.keyname
        print cmd113
        p113 = Popen(cmd113, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        command113out = p113.stdout.read()
        print command113out

    def test_D_import_key_pair_compute(self):
 
        cmd114 = "jcs compute import-key-pair --key-name "+self.keyname+ " --public-key-material "+self.publicKey
        print cmd114 
        p114 = Popen(cmd114, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        command114out = p114.stdout.read()
        print command114out

    def test_E_vpc_subnet_for_compute(self):

    	cmd115 = "jcs vpc create-vpc --cidr-block " +str(self.vpccidr)+" | grep -v 'Request-Id: req'"
    	cmd116 = "jcs vpc describe-vpcs | grep -v 'Request-Id: req'"
    	p115 = Popen(cmd115, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command115out = p115.stdout.read()
    	print command115out

    	p116 = Popen(cmd116, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	vpcjson = p116.stdout.read()
    	print vpcjson
        global vpc_id
    	vpc_id = get_vpc_id(vpcjson)
    	print vpc_id

    	cmd117 = "jcs vpc create-subnet --vpc-id " +str(vpc_id)+ " --cidr-block "+str(self.subnetcidr)+" | grep -v 'Request-Id: req'"
    	print cmd117
    	p117 = Popen(cmd117, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command117out = p117.stdout.read()
    	print command117out

    	cmd118 = "jcs vpc describe-subnets | grep -v 'Request-Id: req'"
    	p118 = Popen(cmd118, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	subnetjson = p118.stdout.read()
    	print subnetjson
        global subnet_id
    	subnet_id = get_subnet_id(subnetjson)
    	command118out = p118.stdout.read()
    	print command118out
        
    def test_F_describe_images_compute(self):
        
    	cmd1 = "jcs compute describe-images | grep -v 'Request-Id: req'" 
    	print cmd1
    	p1 = Popen(cmd1, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	imagejson = p1.stdout.read()
    	print imagejson
        global image_id
    	image_id = get_image_id(imagejson)
    	print image_id

    def test_G_describe_instance_types_compute(self):

    	cmd2 = "jcs compute describe-instance-types | grep -v 'Request-Id: req'"
    	print cmd2
    	p2 = Popen(cmd2, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	instancetypejson = p2.stdout.read()
    	print instancetypejson
        global instancetype_id
    	instancetype_id = get_instancetype_id(instancetypejson)
    	print instancetype_id

    def test_H_run_instances_compute(self):

    	cmd3 = "jcs compute run-instances --image-id " +str(image_id)+ " --instance-type-id " +str(instancetype_id) 
    	print cmd3
    	p3 = Popen(cmd3, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command3out = p3.stdout.read()
    	print command3out

    	time.sleep(10)

    def test_I_describe_instances_compute(self):

    	cmd4 = "jcs compute describe-instances | grep -v 'Request-Id: req'"
    	print cmd4
    	p4 = Popen(cmd4, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        global volume_id
        global instance_id

    	instancevolumejson = p4.stdout.read()
    	print instancevolumejson

    	volume_id = get_volume_id(instancevolumejson)
    	print volume_id

    	instance_id = get_instance_id(instancevolumejson)
    	print instance_id

    def test_JZ_describe_volumes_compute(self):
    	 
    	cmd5 = "jcs compute describe-volumes | grep -v 'Request-Id: req'"
    	print cmd5
    	p5 = Popen(cmd5, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command5out = p5.stdout.read()
    	print command5out

    def test_J_update_delete_on_termination_flag_compute(self):

    	cmd6 = "jcs compute update-delete-on-termination-flag --volume-id " +str(volume_id)+ " --delete-on-termination True"
    	print cmd6
    	p6 = Popen(cmd6, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command6out = p6.stdout.read()
    	print command6out

    def test_K_show_delete_on_termination_flag_compute(self):

    	cmd61 = "jcs compute show-delete-on-termination-flag --volume-id " +str(volume_id)
    	print cmd61
    	p61 = Popen(cmd61, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command61out = p61.stdout.read()
    	print command61out

    	time.sleep(10)

    def test_L_stop_instances_compute(self):

    	cmd8 = "jcs compute stop-instances --instance-ids " +str(instance_id)
    	print cmd8
    	p8 = Popen(cmd8, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command8out = p8.stdout.read()
    	print command8out

	time.sleep(25)

    def test_M_create_snapshot_compute(self):

    	cmd81 = "jcs compute create-snapshot --volume-id " +str(volume_id)
    	print cmd81
    	p81 = Popen(cmd81, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command81out = p81.stdout.read()
    	print command81out

        time.sleep(25)

    def test_N_describe_snapshots_compute(self):

    	cmd82 = "jcs compute describe-snapshots | grep -v 'Request-Id: req'"
    	print cmd82
    	p82 = Popen(cmd82, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	snapshotjson = p82.stdout.read()
    	print snapshotjson
        global snapshot_id
    	snapshot_id = get_snapshot_id(snapshotjson)
    	print snapshot_id

    	time.sleep(25)

    def test_O_start_instances_compute(self):

    	cmd9 = "jcs compute start-instances --instance-ids " +str(instance_id)
    	print cmd9
    	p9 = Popen(cmd9, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command9out = p9.stdout.read()
    	print command9out

    	time.sleep(55)

    def test_P_create_volume_compute(self):

    	cmd91 = "jcs compute create-volume --size 1 | grep -v 'Request-Id: req'"
    	print cmd91
    	p91 = Popen(cmd91, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	sbsvolumejson = p91.stdout.read()
    	print sbsvolumejson
        global sbsvolume_id
    	sbsvolume_id = get_sbsvolume_id(sbsvolumejson)
    	print sbsvolume_id

    	time.sleep(10)
 
    def test_Q_attach_volume_compute(self):

    	cmd93 = "jcs compute attach-volume --volume-id " +str(sbsvolume_id)+ " --instance-id " +str(instance_id)+ " --device /dev/vdb"
    	print cmd93
    	p93 = Popen(cmd93, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command93out = p93.stdout.read()
    	print command93out

    	time.sleep(35)

    def test_R_reboot_instances_compute(self):

    	cmd711 = "jcs compute reboot-instances --instance-ids "+str(instance_id)
    	print cmd711
    	p711 = Popen(cmd711, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command711out = p711.stdout.read()
    	print command711out

    	time.sleep(55)

    def test_S_detach_volume_compute(self):

    	cmd94 = "jcs compute detach-volume --volume-id " +str(sbsvolume_id)+ " --instance-id " +str(instance_id)
    	print cmd94
    	p94 = Popen(cmd94, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command94out = p94.stdout.read()
    	print command94out

    def test_T_delete_volume_compute(self):

    	cmd95 = "jcs compute delete-volume --volume-id " +str(sbsvolume_id)
    	print cmd95
    	p95 = Popen(cmd95, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command95out = p95.stdout.read()
    	print command95out

    def test_U_delete_snapshot_compute(self):

    	cmd83 = "jcs compute delete-snapshot --snapshot-id " +str(snapshot_id)
    	print cmd83
    	p83 = Popen(cmd83, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command83out = p83.stdout.read()
    	print command83out

    def test_V_allocate_address_compute(self):

    	cmd84 = "jcs vpc allocate-address --domain vpc | grep -v 'Request-Id: req'"
    	print cmd84
    	p84 = Popen(cmd84, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command84out = p84.stdout.read()
    	print command84out

    def test_W_describe_addresses_compute(self):

    	cmd85 = "jcs vpc describe-addresses | grep -v 'Request-Id: req'"
    	print cmd85
    	p85 = Popen(cmd85, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	addressesjson = p85.stdout.read()
    	print addressesjson
        global allocation_id
    	allocation_id = get_allocation_id(addressesjson)
    	print allocation_id

    def test_X_associate_address_compute(self):


    	cmd86 = "jcs vpc associate-address --instance-id "+str(instance_id)+" --allocation-id " +str(allocation_id)+ " | grep -v 'Request-Id: req'"
    	print cmd86
    	p86 = Popen(cmd86, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	associatejson = p86.stdout.read()
        global association_id
    	print associatejson
    	association_id = get_association_id(associatejson)
    	print association_id

    	time.sleep(10)
    
    def test_Y_disassociate_address_compute(self):

    	cmd87 = "jcs vpc disassociate-address --association-id " +str(association_id)
    	print cmd87
    	p87 = Popen(cmd87, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command87out = p87.stdout.read()
    	print command87out

    def test_ZA_release_address_compute(self):

    	cmd88 = "jcs vpc release-address --allocation-id " +str(allocation_id)
    	print cmd88
    	p88 = Popen(cmd88, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command88out = p88.stdout.read()
    	print command88out

    def test_ZB_describe_route_tables_compute(self):

    	cmd89 = "jcs vpc describe-route-tables | grep -v 'Request-Id: req'"
    	print cmd89
    	p89 = Popen(cmd89, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	routetablejson = p89.stdout.read()
    	print routetablejson
        global routetable_id
    	routetable_id = get_routetableforroute_id(routetablejson)
    	print routetable_id

    def test_ZC_create_route_compute(self):

    	cmd90 = "jcs vpc create-route --route-table-id " +str(routetable_id)+ " --destination-cidr-block 100.0.0.100/32 --instance-id "+str(instance_id)
    	print cmd90
    	p90 = Popen(cmd90, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command90out = p90.stdout.read()
    	print command90out

    	time.sleep(10)

    def test_ZD_delete_route_compute(self):

    	cmd91 = "jcs vpc delete-route --route-table-id " +str(routetable_id)+ " --destination-cidr-block 100.0.0.100/32"
    	print cmd91
    	p91 = Popen(cmd91, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command91out = p91.stdout.read()
    	print command91out

    def test_ZE_get_password_data_compute(self):

        cmd92 = "jcs compute get-password-data --instance-id "+str(instance_id)
        print cmd92
        p92 = Popen(cmd92, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        command92out = p92.stdout.read()
        print command92out


    def test_ZF_terminate_instances_compute(self):

    	cmd10 = "jcs compute terminate-instances --instance-ids " +str(instance_id)
    	print cmd10
    	p10 = Popen(cmd10, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command10out = p10.stdout.read()
    	print command10out
        time.sleep(20)

    def test_ZG_delete_subnet_compute(self):

    	cmd11 = "jcs vpc delete-subnet --subnet-id "+str(subnet_id)
    	print cmd11
    	p11 = Popen(cmd11, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command11out = p11.stdout.read()
    	print command11out

    def test_ZH_delete_vpc_compute(self):

    	cmd12 = "jcs vpc delete-vpc --vpc-id " +str(vpc_id)
    	print cmd12
    	p12 = Popen(cmd12, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command12out = p12.stdout.read()
    	print command12out
        

if __name__ == '__main__':

    unittest.main()
    HTMLTestRunner.main()



