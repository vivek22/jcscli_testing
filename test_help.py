#!/usr/bin/python

__author__= " VIVEK SHARMA "

import os
import sys
import re
import string
import time
import ConfigParser
import datetime
from subprocess import Popen, PIPE, STDOUT
import subprocess
import HTMLTestRunner
import unittest



class HelpTest(unittest.TestCase):

    def test_jcs_help(self):
        command = "jcs help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_vpc_help(self):
        command = "jcs vpc help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_vpc_help(self):
        command = "jcs vpc create-vpc help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_vpc_help(self):
        command = "jcs vpc delete-vpc help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_vpcs_help(self):
        command = "jcs vpc describe-vpcs help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_subnet_help(self):
        command = "jcs vpc create-subnet help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_subnet_help(self):
        command = "jcs vpc help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_subnets_help(self):
        command = "jcs vpc describe-subnets help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_security_group_help(self):
        command = "jcs vpc create-security-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_authorize_security_group_ingress_help(self):
        command = "jcs vpc authorize-security-group-ingress help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_authorize_security_group_egress_help(self):
        command = "jcs vpc authorize-security-group-egress help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_revoke_security_group_ingress_help(self):
        command = "jcs vpc revoke-security-group-ingress help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_revoke_security_group_egress_help(self):
        command = "jcs vpc revoke-security-group-egress help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_security_groups_help(self):
        command = "jcs vpc describe-security-groups help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_security_group_help(self):
        command = "jcs vpc delete-security-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_route_help(self):
        command = "jcs vpc create-route help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_delete_route_help(self):
        command = "jcs vpc delete-route help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_route_table_help(self):
        command = "jcs vpc create-route-table help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_route_table_help(self):
        command = "jcs vpc delete-route-table help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_associate_route_table_help(self):
        command = "jcs vpc associate-route-table help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_disassociate_route_table_help(self):
        command = "jcs vpc disassociate-route-table help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_route_tables_help(self):
        command = "jcs vpc describe-route-tables help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_allocate_address_help(self):
        command = "jcs vpc allocate-address help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_associate_address_help(self):
        command = "jcs vpc associate-address help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_disassociate_address_help(self):
        command = "jcs vpc disassociate-address help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_release_address_help(self):
        command = "jcs vpc release-address help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_addresses_help(self):
        command = "jcs vpc describe-addresses help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_compute_help(self):
        command = "jcs compute help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_images_help(self):
        command = "jcs compute describe-images help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_instance_types_help(self):
        command = "jcs compute describe-instance-types help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_key_pair_help(self):
        command = "jcs compute create-key-pair help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_key_pairs_help(self):
        command = "jcs compute describe-key-pairs help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_import_key_pair_help(self):
        command = "jcs compute import-key-pair help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_key_pair_help(self):
        command = "jcs compute delete-key-pair help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_run_instances_help(self):
        command = "jcs compute run-instances help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_instances_help(self):
        command = "jcs compute describe-instances help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_start_instances_help(self):
        command = "jcs compute start-instances help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_stop_instances_help(self):
        command = "jcs compute stop-instances help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_reboot_instances_help(self):
        command = "jcs compute reboot-instances help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_terminate_instances_help(self):
        command = "jcs compute terminate-instances help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_snapshot_help(self):
        command = "jcs compute create-snapshot help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_snapshots_help(self):
        command = "jcs compute describe-snapshots help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_snapshot_help(self):
        command = "jcs compute delete-snapshot help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_show_delete_on_termination_flag_help(self):
        command = "jcs compute show-delete-on-termination-flag help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_update_delete_on_termination_flag_help(self):
        command = "jcs compute update-delete-on-termination-flag help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_password_data_help(self):
        command = "jcs compute get-password-data help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_attach_volume_help(self):
        command = "jcs compute attach-volume help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_volume_help(self):
        command = "jcs compute create-volume help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_volumes_help(self):
        command = "jcs compute describe-volumes help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_detach_volume_help(self):
        command = "jcs compute detach-volume help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_volume_help(self):
        command = "jcs compute delete-volume help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_rds_help(self):
        command = "jcs rds help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_db_instance_help(self):
        command = "jcs rds create-db-instance help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_db_snapshot_help(self):
        command = "jcs rds create-db-snapshot help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_db_instance_help(self):
        command = "jcs rds delete-db-instance help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_db_snapshot_help(self):
        command = "jcs rds delete-db-snapshot help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_db_instances_help(self):
        command = "jcs rds describe-db-instances help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_describe_db_snapshots_help(self):
        command = "jcs rds describe-db-snapshots help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_modify_db_instance_help(self):
        command = "jcs rds modify-db-instance help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_restore_db_instance_from_db_snapshot_help(self):
        command = "jcs rds restore-db-instance-from-db-snapshot help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_iam_help(self):
        command = "jcs iam help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_list_users_help(self):
        command = "jcs iam list-users help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_user_help(self):
        command = "jcs iam create-user help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_update_user_help(self):
        command = "jcs iam update-user help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_user_help(self):
        command = "jcs iam delete-user help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_user_help(self):
        command = "jcs iam get-user help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_user_summary_help(self):
        command = "jcs iam get-user-summary help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_credential_help(self):
        command = "jcs iam create-credential help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_credential_help(self):
        command = "jcs iam delete-credential help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_user_credential_help(self):
        command = "jcs iam get-user-credential help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_list_groups_help(self):
        command = "jcs iam list-groups help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_group_help(self):
        command = "jcs iam create-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_group_help(self):
        command = "jcs iam get-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_delete_group_help(self):
        command = "jcs iam delete-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_assign_user_to_group_help(self):
        command = "jcs iam assign-user-to-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_check_user_in_group_help(self):
        command = "jcs iam check-user-in-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_remove_user_from_group_help(self):
        command = "jcs iam remove-user-from-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_list_groups_for_user_help(self):
        command = "jcs iam list-groups-for-user help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_list_user_in_group_help(self):
        command = "jcs iam list-user-in-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_update_group_help(self):
        command = "jcs iam update-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_group_summary_help(self):
        command = "jcs iam get-group-summary help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_policy_help(self):
        command = "jcs iam create-policy help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_policy_help(self):
        command = "jcs iam get-policy help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_list_policies_help(self):
        command = "jcs iam list-policies help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_policy_help(self):
        command = "jcs iam delete-policy help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_update_policy_help(self):
        command = "jcs iam update-policy help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_attach_policy_to_user_help(self):
        command = "jcs iam attach-policy-to-user help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_detach_policy_from_user_help(self):
        command = "jcs iam detach-policy-from-user help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_attach_policy_to_group_help(self):
        command = "jcs iam attach-policy-to-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_detach_policy_from_group_help(self):
        command = "jcs iam detach-policy-from-group help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_policy_summary_help(self):
        command = "jcs iam get-policy-summary help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_resource_based_policy_help(self):
        command = "jcs iam create-resource-based-policy help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_resource_based_policy_help(self):
        command = "jcs iam get-resource-based-policy help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_list_resource_based_policies_help(self):
        command = "jcs iam list-resource-based-policies help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_resource_based_policy_help(self):
        command = "jcs iam delete-resource-based-policy help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_update_resource_based_policy_help(self):
        command = "jcs iam update-resource-based-policy help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_attach_policy_to_resource_help(self):
        command = "jcs iam attach-policy-to-resource help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_detach_policy_from_resource_help(self):
        command = "jcs iam detach-policy-from-resource help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_dss_help(self):
        command = "jcs dss help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_bucket_help(self):
        command = "jcs dss create-bucket help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_copy_object_help(self):
        command = "jcs dss copy-object help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_bucket_help(self):
        command = "jcs dss delete-bucket help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_delete_object_help(self):
        command = "jcs dss delete-object help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_object_help(self):
        command = "jcs dss get-object help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_head_bucket_help(self):
        command = "jcs dss head-bucket help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_list_buckets_help(self):
        command = "jcs dss list-buckets help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_list_objects_help(self):
        command = "jcs dss list-objects help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_put_object_help(self):
        command = "jcs dss put-object help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_get_presigned_url_help(self):
        command = "jcs dss get-presigned-url help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_create_multipart_upload_help(self):
        command = "jcs dss create-multipart-upload help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_upload_part_help(self):
        command = "jcs dss upload-part help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_list_parts_help(self):
        command = "jcs dss list-parts help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_complete_multipart_upload_help(self):
        command = "jcs dss complete-multipart-upload help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_abort_multipart_upload_help(self):
        command = "jcs dss abort-multipart-upload help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_list_multipart_uploads_help(self):
        command = "jcs dss list-multipart-uploads help"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout


if __name__ == '__main__':
    unittest.main()
    HTMLTestRunner.main()
