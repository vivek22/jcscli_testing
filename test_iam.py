#!/usr/bin/python

__author__= " VIVEK SHARMA "

import os
import sys
import re
import string
import time
import ConfigParser
import datetime
from subprocess import Popen, PIPE, STDOUT
import subprocess
import random
import unittest
import ConfigParser
from modulevpcids import *
from modulecomputeids import *
from moduleiamids import *

class IamTest(unittest.TestCase):

    configParser = ConfigParser.RawConfigParser()
    configFilePath = r'cliconfig'
    configParser.read(configFilePath)
    username = configParser.get('IAM', 'username')
    groupname = configParser.get('IAM', 'groupname')
    
    def test_A_create_user_iam(self):
    
    	cmd1 = "jcs iam create-user --name "+self.username
    	print cmd1
    	p1 = Popen(cmd1, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command1out = p1.stdout.read()
    	print command1out

    def test_B_list_users_iam(self):

    	cmd2 = "jcs iam list-users "
    	print cmd2
    	p2 = Popen(cmd2, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command2out = p2.stdout.read()
    	print command2out
    
    def test_C_update_user_iam(self):

    	newpassword = "ReliAn12!33" 
    	shufflednewpassword = "".join(random.sample(newpassword, len(newpassword)))

    	cmd3 = "jcs iam update-user --name "+self.username+ " --new-password "+str(shufflednewpassword)
    	p3 = Popen(cmd3, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command3out = p3.stdout.read()
    	print command3out
    
    def test_D_get_user_iam(self):

    	cmd4 = "jcs iam get-user --name "+self.username 
    	p4 = Popen(cmd4, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command4out = p4.stdout.read()
    	print command4out

    def test_E_get_user_summary_iam(self):

    	cmd5 = "jcs iam get-user-summary --name "+self.username
    	p5 = Popen(cmd5, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command5out = p5.stdout.read()
    	print command5out

    def test_F_create_group_iam(self):

    	cmd7 = "jcs iam create-group --name "+self.groupname
    	print cmd7
    	p7 = Popen(cmd7, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command7out = p7.stdout.read()
    	print command7out

    def test_G_list_groups_iam(self):

    	cmd8 = "jcs iam list-groups "
    	print cmd8
    	p8 = Popen(cmd8, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command8out = p8.stdout.read()
    	print command8out
        
    def test_H_get_group_iam(self):

    	cmd9 = "jcs iam get-group --name "+self.groupname
    	p9 = Popen(cmd9, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command9out = p9.stdout.read()
    	print command9out

    	time.sleep(20)

    def test_I_create_policy_iam(self):

    	policydocument = '"{\\\"name\\\": \\\"test\\\", \\\"statement\\\": [{\\\"action\\\": [\\\"jrn:jcs:dss:*\\\"], \\\"resource\\\": [\\\"jrn:jcs:dss::*:*\\\"], \\\"effect\\\": \\\"allow\\\"}]}"'
        print policydocument
    	cmd11 = "jcs iam create-policy --policy-document "+policydocument
        print cmd11
    	p11 = Popen(cmd11, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command11out = p11.stdout.read()
    	print command11out

    	time.sleep(10)

    def test_J_get_policy_and_update_policy_iam(self):

    	cmd12 = "jcs iam get-policy --name test"
    	p12 = Popen(cmd12, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command12out = p12.stdout.read()
    	print command12out

	''''        
    	updatedpolicydocument = "{\\\"name\\\": \\\"test\\\", \\\"statement\\\": [{\\\"action\\\": [\\\"jrn:jcs:dss:*\\\"], \\\"resource\\\": [\\\"jrn:jcs:dss::*:*\\\"], \\\"effect\\\": \\\"allow\\\"}]}"
    	
	cmd13 = "jcs iam update-policy --policy-document "+updatedpolicydocument+" --name test"  
    	p13 = Popen(cmd13, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command13out = p13.stdout.read()
    	print command13out

        '''

    def test_K_list_policies_iam(self):

    	cmd14 = "jcs iam list-policies"
    	p14 = Popen(cmd14, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command14out = p14.stdout.read()
    	print command14out
    	
       
    def test_L_create_credential_iam(self):

    	cmd16 = "jcs iam create-credential --user-name "+self.username
    	p16 = Popen(cmd16, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command16out = p16.stdout.read()
    	print command16out

    def test_M_get_user_delete_credential_iam(self):

    	cmd17 = "jcs iam get-user-credential --user-name "+self.username+" | grep -v 'Request-Id: req-'"
    	p17 = Popen(cmd17, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	accesskeyjson = p17.stdout.read()
    	print accesskeyjson
        global accesskey_value
    	accesskey_value = get_accesskey_value(accesskeyjson)
    	print accesskey_value

    def test_N_get_user_delete_credential_iam(self):

    	cmd18 = "jcs iam delete-credential --access-key "+accesskey_value
    	p18 = Popen(cmd18, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command18out = p18.stdout.read()
    	print command18out

    def test_O_assign_user_to_group_iam(self):

   	cmd19 = "jcs iam assign-user-to-group --user-name "+self.username+" --group-name "+self.groupname
    	p19 = Popen(cmd19, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command19out = p19.stdout.read()
    	print command19out

    def test_P_check_user_in_group_iam(self):

    	cmd20 = "jcs iam check-user-in-group --user-name "+self.username+" --group-name "+self.groupname
    	p20 = Popen(cmd20, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command20out = p20.stdout.read()
    	print command20out

    def test_Q_list_groups_for_user_iam(self):

    	cmd21 = "jcs iam list-groups-for-user --name "+self.username
    	p21 = Popen(cmd21, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command21out = p21.stdout.read()
    	print command21out

    def test_R_list_user_in_group_iam(self):

    	cmd22 = "jcs iam list-user-in-group --name "+self.groupname
    	p22 = Popen(cmd22, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command22out = p22.stdout.read()
    	print command22out

    def test_S_update_group_iam(self): 

    	updategroupname = "test"
    	cmd23 = "jcs iam update-group --name "+self.groupname+" --new-name "+updategroupname+" --new-description 'The new description of the group' "
    	p23 = Popen(cmd23, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command23out = p23.stdout.read()
    	print command23out

    def test_T_get_group_summary_iam(self):

    	cmd24 = "jcs iam get-group-summary --name "+self.groupname
    	p24 = Popen(cmd24, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command24out = p24.stdout.read()
    	print command24out

    def test_U_attach_policy_to_user_iam(self):
    	
    	cmd26 = "jcs iam attach-policy-to-user --policy-name test --user-name "+self.username
    	p26 = Popen(cmd26, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command26out = p26.stdout.read()
    	print command26out
    
    def test_V_detach_policy_from_user_iam(self):

    	cmd27 = "jcs iam detach-policy-from-user --policy-name test --user-name "+self.username 
    	p27 = Popen(cmd27, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command27out = p27.stdout.read()
    	print command27out

    def test_W_attach_policy_to_group_iam(self):

    	cmd28 = "jcs iam attach-policy-to-group --policy-name test --group-name "+self.groupname
    	p28 = Popen(cmd28, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command28out = p28.stdout.read()
    	print command28out

    def test_X_detach_policy_from_group_iam(self):

    	cmd29 = "jcs iam detach-policy-from-group --policy-name test --group-name "+self.groupname
    	p29 = Popen(cmd29, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command29out = p29.stdout.read()
    	print command29out

    def test_Y_get_policy_summary_iam(self):

    	cmd30 = "jcs iam get-policy-summary --name test"
    	p30 = Popen(cmd30, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command30out = p30.stdout.read()
    	print command30out

    def test_ZA_create_resource_based_policy_iam(self):

        global policydocumentRBP
    	policydocumentRBP = '"{\\\"name\\\": \\\"testrbp\\\", \\\"statement\\\": [{\\\"action\\\": [\\\"jrn:jcs:dss:ListBucket\\\"], \\\"principle\\\": [\\\"jrn:jcs:iam:091060158379:User:vivek1\\\"], \\\"effect\\\": \\\"allow\\\"}]}"'
        print policydocumentRBP
    	cmd31 = "jcs iam create-resource-based-policy --policy-document "+policydocumentRBP
    	p31 = Popen(cmd31, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command31out = p31.stdout.read()
    	print command31out

    def test_ZB_list_resource_based_policies_iam(self):

    	cmd32 = "jcs iam list-resource-based-policies"
    	p32 = Popen(cmd32, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command32out = p32.stdout.read()
    	print command32out

    def test_ZC_update_resource_based_policy_iam(self):

    	cmd33 = "jcs iam update-resource-based-policy --policy-document "+policydocumentRBP+" --name testrbp"
    	p33 = Popen(cmd33, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command33out = p33.stdout.read()
    	print command33out

    def test_ZD_get_resource_based_policy_iam(self):

    	cmd34 = "jcs iam get-resource-based-policy --name testrbp"
    	p34 = Popen(cmd34, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command34out = p34.stdout.read()
    	print command34out

	''''

    def test_ZE_attach_policy_to_resource_iam(self):

        global resource_value
    	resource_value = '"{\\\"resource\\\":[\\\"jrn:jcs:dss:649238181254:*\\\"]}"'

    	cmd35 = "jcs iam attach-policy-to-resource --policy-name testrbp --resource "+resource_value
        cmd35 = "jcs iam attach-policy-to-resource --policy-name testrbp --resource "+resource_value
    	p35 = Popen(cmd35, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command35out = p35.stdout.read()
    	print command35out

    	time.sleep(10)

    def test_ZF_detach_policy_from_resource_iam(self):

    	cmd36 = "jcs iam detach-policy-from-resource --policy-name testrbp --resource "+resource_value
        cmd36 = "jcs iam detach-policy-from-resource --policy-name testrbp --resource "+resource_value
    	p36 = Popen(cmd36, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command36out = p36.stdout.read()
    	print command36out

	'''

    def test_ZG_get_resource_based_policy_summary_iam(self):

    	cmd37 = "jcs iam get-resource-based-policy-summary --name testrbp"
    	p37 = Popen(cmd37, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command37out = p37.stdout.read()
    	print command37out
    
    def test_ZH_delete_policy_iam(self):

    	cmd15 = "jcs iam delete-policy --name test "
    	p15 = Popen(cmd15, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command15out = p15.stdout.read()
    	print command15out

    def test_ZI_delete_resource_based_policy_iam(self):

        cmd38 = "jcs iam delete-resource-based-policy --name testrbp"
        p38 = Popen(cmd38, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        command38out = p38.stdout.read()
        print command38out

        	
    def test_ZJ_remove_user_from_group_iam(self):

    	cmd25 = "jcs iam remove-user-from-group --user-name "+self.username+" --group-name "+self.groupname
    	p25 = Popen(cmd25, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command25out = p25.stdout.read()
    	print command25out

    def test_ZK_delete_group_iam(self):

    	cmd10 = "jcs iam delete-group --name "+self.groupname
    	p10 = Popen(cmd10, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command10out = p10.stdout.read()
    	print command10out

    def test_ZL_delete_user_iam(self):

    	cmd6 = "jcs iam delete-user --name "+self.username
    	p6 = Popen(cmd6, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command6out = p6.stdout.read()
    	print command6out


if __name__ == '__main__':

    unittest.main()
    HTMLTestRunner.main()



