#!/usr/bin/python

__author__= " VIVEK SHARMA "

import os
import sys
import re
import string
import time
import ConfigParser
import datetime
from subprocess import Popen, PIPE, STDOUT
import subprocess
import unittest
import ConfigParser
from modulevpcids import *
from moduleiamids import *
from modulecomputeids import *


class VpcTest(unittest.TestCase):

    configParser = ConfigParser.RawConfigParser()
    configFilePath = r'cliconfig'
    configParser.read(configFilePath)
    vpccidr = configParser.get('VPC', 'vpccidr')
    subnetcidr = configParser.get('VPC', 'subnetcidr')

    def test_A_create_vpc(self):

        cmd1 = "jcs vpc create-vpc --cidr-block " +str(self.vpccidr)+" | grep -v 'Request-Id: req'"
        p1 = Popen(cmd1, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        command1out = p1.stdout.read()
        print command1out

    def test_B_describe_vpc(self):

        cmd2 = "jcs vpc describe-vpcs | grep -v 'Request-Id: req'"
        p2 = Popen(cmd2, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        myjson = p2.stdout.read()
        print myjson
        global vpc_id
        vpc_id = get_vpc_id(myjson)
        print vpc_id

    def test_C_create_subnet(self):

    	cmd3 = "jcs vpc create-subnet --vpc-id " +str(vpc_id)+ " --cidr-block "+str(self.subnetcidr)+" | grep -v 'Request-Id: req'"
    	print cmd3
    	p3 = Popen(cmd3, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command3out = p3.stdout.read()
    	print command3out

    def test_D_describe_subnet(self):

    	cmd4 = "jcs vpc describe-subnets | grep -v 'Request-Id: req'"
    	p4 = Popen(cmd4, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	subnetjson = p4.stdout.read()
    	print subnetjson
        global subnet_id
    	subnet_id = get_subnet_id(subnetjson)
    	command4out = p4.stdout.read()
    	print command4out

    def test_E_create_securitygroup(self):

    	cmd3 = "jcs vpc create-security-group --group-name WebServerSG --group-description Web-Servers --vpc-id " +str(vpc_id)
    	p3 = Popen(cmd3, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command3out = p3.stdout.read()
        print command3out

    def test_F_describe_securitygroups(self):

    	cmd4 = "jcs vpc describe-security-groups | grep -v 'Request-Id: req'"
    	p4 = Popen(cmd4, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	securitygroupjson = p4.stdout.read()
    	print securitygroupjson
        global securitygroup_id
    	securitygroup_id = get_securitygroup_id(securitygroupjson)
    	print securitygroup_id

    def test_G_authorize_security_group_ingress_securitygroup(self):

    	cmd5= "jcs vpc authorize-security-group-ingress --group-id " +str(securitygroup_id)+ " --protocol tcp --port 80 --cidr 10.0.0.0/24"
    	p5 = Popen(cmd5, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command5out = p5.stdout.read()
    	print command5out

    def test_H_revoke_security_group_ingress_securitygroup(self):

    	cmd6= "jcs vpc revoke-security-group-ingress --group-id " +str(securitygroup_id)+ " --protocol tcp --port 80 --cidr 10.0.0.0/24"
    	p6 = Popen(cmd6, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command6out = p6.stdout.read()
    	print command6out

    def test_I_authorize_security_group_egress_securitygroup(self):

    	cmd7= "jcs vpc authorize-security-group-egress --group-id " +str(securitygroup_id)+ " --protocol tcp --port 80 --cidr 10.0.0.0/24"
    	p7 = Popen(cmd7, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command7out = p7.stdout.read()
    	print command7out

    def test_J_revoke_security_group_egress_securitygroup(self):

    	cmd8= "jcs vpc revoke-security-group-egress --group-id " +str(securitygroup_id)+ " --protocol tcp --port 80 --cidr 10.0.0.0/24"
    	p8 = Popen(cmd8, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command8out = p8.stdout.read()
    	print command8out

    def test_K_delete_securitygroup(self):

    	cmd9= "jcs vpc delete-security-group --group-id " +str(securitygroup_id) 
    	p9 = Popen(cmd9, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command9out = p9.stdout.read()
    	print command9out

    def test_L_create_routetable(self):

    	cmd5 = "jcs vpc create-route-table --vpc-id " +str(vpc_id)+" | grep -v 'Request-Id: req'"
    	print cmd5
    	p5 = Popen(cmd5, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	routetablejson = p5.stdout.read()
        global routetable_id
    	routetable_id = get_routetable_id(routetablejson)
    	print routetable_id

    def test_M_describe_routetable(self):

    	cmd61 = "jcs vpc describe-route-tables | grep -v 'Request-Id: req'"
    	p61 = Popen(cmd61, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command61out = p61.stdout.read()
    	print command61out

    def test_N_associate_route_table_routetable(self):

    	cmd7 = "jcs vpc associate-route-table --route-table-id " +str(routetable_id)+ " --subnet-id " +str(subnet_id)+" | grep -v 'Request-Id: req'"
    	p7 = Popen(cmd7, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	routetableassociationjson = p7.stdout.read()
    	print routetableassociationjson
        global routetableassociation_id
    	routetableassociation_id = get_routetableassociation_id(routetableassociationjson)
    	print routetableassociation_id
    	command7out = p7.stdout.read()
    	print command7out

    def test_O_disassociate_route_table_routetable(self):

    	cmd8 = "jcs vpc disassociate-route-table --association-id " +str(routetableassociation_id)+ " | grep -v 'Request-Id: req'"
    	p8 = Popen(cmd8, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command8out = p8.stdout.read()
    	print command8out

    def test_P_delete_route_table_routetable(self):

    	cmd9 = "jcs vpc delete-route-table --route-table-id " +str(routetable_id)+" | grep -v 'Request-Id: req'"
    	p9 = Popen(cmd9, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command9out = p9.stdout.read()
    	print command9out

    def test_Q_allocate_address(self):

    	cmd1 = "jcs vpc allocate-address --domain vpc | grep -v 'Request-Id: req'"
    	print cmd1
    	p1 = Popen(cmd1, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command1out = p1.stdout.read()
    	print command1out

    def test_R_describe_addresses(self):

    	cmd2 = "jcs vpc describe-addresses | grep -v 'Request-Id: req'"
    	print cmd2
    	p2 = Popen(cmd2, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	addressesjson = p2.stdout.read()
    	print addressesjson
        global allocation_id
    	allocation_id = get_allocation_id(addressesjson)
    	print allocation_id

    def test_S_release_address(self):

    	cmd3 = "jcs vpc release-address --allocation-id " +str(allocation_id)+ " | grep -v 'Request-Id: req'"
    	print cmd3
    	p3 = Popen(cmd3, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command3out = p3.stdout.read()
    	print command3out

    def test_T_delete_subnet(self):

        cmd5 = "jcs vpc delete-subnet --subnet-id " +str(subnet_id)+" | grep -v 'Request-Id: req'"
        p5 = Popen(cmd5, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        command5out = p5.stdout.read()
        print command5out

    def test_U_delete_vpc(self):

        cmd3 = "jcs vpc delete-vpc --vpc-id " +str(vpc_id)+" | grep -v 'Request-Id: req'"
        p3 = Popen(cmd3, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        command3out = p3.stdout.read()
        print command3out


    def responsecapture(ret,command):

    	vpcf=0
    	vpcp=0

    	if ret !=0:
        	print 'fail of command',command
        	vpcf = vpcf + 1
    	else:
        	print 'pass of command',command
        	vpcp = vpcp + 1

    	print vpcf
    	print vpcp




if __name__ == '__main__':

    unittest.main()
    HTMLTestRunner.main()


