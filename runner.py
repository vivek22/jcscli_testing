#!/usr/bin/python

import HTMLTestRunner
import unittest, time
import time
import os 
import string
from test_dss import DssTest 
from test_iam import IamTest
from test_compute import ComputeTest
from test_vpc import VpcTest
from test_rds import RdsTest
from test_help import HelpTest

def main():

	print "Starting execution: runner.py"
  	Dss1 = unittest.TestLoader().loadTestsFromTestCase(DssTest)
        Iam1 = unittest.TestLoader().loadTestsFromTestCase(IamTest)
	Compute1 = unittest.TestLoader().loadTestsFromTestCase(ComputeTest)
        Vpc1 = unittest.TestLoader().loadTestsFromTestCase(VpcTest)
        Rds1 = unittest.TestLoader().loadTestsFromTestCase(RdsTest)
        Help1 = unittest.TestLoader().loadTestsFromTestCase(HelpTest)

	suite = unittest.TestSuite([Iam1])
#	suite = unittest.TestSuite([Dss1,Iam1,Compute1,Vpc1,Rds1,Help1])

	currentdate = time.strftime("%d%m%Y-%H")
        filename= str(currentdate)+".html"
	
	print "Staring Test Suite :" + currentdate

        output = open("main.html","w")
        runner = HTMLTestRunner.HTMLTestRunner(stream=output,title='JCS-CLI Test Report')
        runner.run(suite)

	print "Completed Test Suide :" + currentdate
        time.sleep(20)

        cmd1 = "cp /home/centos/main.html /var/www/html/"
        os.system(cmd1)

        cmd2 = "cp /var/www/html/main.html /var/www/html/" + str(filename)
        os.system(cmd2)

	print "You can access test report at http://10.140.201.79/main.html"


if __name__ == "__main__":
#        os.environ["ACCESS_KEY"]="e5a2d6d51cb741eb8ec95d7bb634de30"
#        os.environ["SECRET_KEY"]="6c32e82f709b48b4a6383c92c61f795c"

	main()
