#!/usr/bin/python

__author__= " VIVEK SHARMA "

import os
import sys
import re
import string
import time
import ConfigParser
import datetime
from subprocess import Popen, PIPE, STDOUT
import subprocess
import ConfigParser
import random
import unittest

class RdsTest(unittest.TestCase):

    configParser = ConfigParser.RawConfigParser()
    configFilePath = r'cliconfig'
    configParser.read(configFilePath)
    masterusername = configParser.get('RDS', 'masterusername')
    masteruserpassword = configParser.get('RDS', 'masteruserpassword')
    allocatedstorage = configParser.get('RDS', 'allocatedstorage')
    toshuffledbinstanceidentifier = configParser.get('RDS', 'toshuffledbinstanceidentifier')
    toshuffledbsnapshotidentifier = configParser.get('RDS', 'toshuffledbsnapshotidentifier')
    restoreddbinstanceidentifier = configParser.get('RDS' , 'restoreddbinstanceidentifier')

#    masterusername = "master"
#    masteruserpassword = "masterpass"
#    allocatedstorage = "20"
#    toshuffledbinstanceidentifier = "abcdefghijk"
#    toshuffledbsnapshotidentifier = "klmnopqrstu"
#    restoreddbinstanceidentifier = "vwxyzABCDEF"

    dbinstanceidentifier = "".join(random.sample(toshuffledbinstanceidentifier, len(toshuffledbinstanceidentifier)))

    dbsnapshotidentifier = "".join(random.sample(toshuffledbsnapshotidentifier, len(toshuffledbsnapshotidentifier)))

    newdbinstanceidentifier = "".join(random.sample(restoreddbinstanceidentifier, len(restoreddbinstanceidentifier)))


    def test_A_create_db_instance_rds(self):

    	cmd1 = "jcs rds create-db-instance --db-instance-identifier "+self.dbinstanceidentifier+" --db-instance-class c1.small --engine MySQL --allocated-storage "+str(self.allocatedstorage)+" --master-username "+self.masterusername+" --master-user-password "+self.masteruserpassword
    	print cmd1
    	p1 = Popen(cmd1, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command1out = p1.stdout.read()
    	print command1out

    	time.sleep(240)

    def test_B_create_db_snapshot_rds(self):

    	cmd2 = "jcs rds create-db-snapshot --db-instance-identifier "+self.dbinstanceidentifier+" --db-snapshot-identifier "+self.dbsnapshotidentifier
    	print cmd2
    	p2 = Popen(cmd2, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command2out = p2.stdout.read()
    	print command2out

    	time.sleep(240)

    def test_C_describe_db_instances_rds(self):

    	cmd3 = "jcs rds describe-db-instances"
    	print cmd3
    	p3 = Popen(cmd3, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command3out = p3.stdout.read()
    	print command3out

    def test_D_describe_db_snapshots_rds(self):

    	cmd4 = "jcs rds describe-db-snapshots"
    	print cmd4
    	p4 = Popen(cmd4, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command4out = p4.stdout.read()
    	print command4out
    
    def test_E_delete_db_instance_rds(self):

    	cmd5 = "jcs rds delete-db-instance --db-instance-identifier "+self.dbinstanceidentifier+" --skip-final-snapshot"
    	print cmd5
    	p5 = Popen(cmd5, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command5out = p5.stdout.read()
    	print command5out
    
    	time.sleep(180)

    def test_F_restore_db_instance_from_db_snapshot_rds(self):

    	cmd6 = "jcs rds restore-db-instance-from-db-snapshot --db-instance-identifier "+self.restoreddbinstanceidentifier+" --db-snapshot-identifier "+self.dbsnapshotidentifier+" --db-instance-class c1.small "
    	print cmd6
    	p6 = Popen(cmd6, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command6out = p6.stdout.read()
    	print command6out

    	time.sleep(240)

    def test_G_modify_db_instance_rds(self):

    	cmd7 = "jcs rds modify-db-instance --db-instance-identifier "+self.restoreddbinstanceidentifier+ " --new-db-instance-identifier "+self.newdbinstanceidentifier
    	print cmd7
    	p7 = Popen(cmd7, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command7out = p7.stdout.read()
    	print command7out

    	time.sleep(15)

    def test_H_delete_db_snapshot_rds(self):
    
    	cmd8 = "jcs rds delete-db-snapshot --db-snapshot-identifier "+self.dbsnapshotidentifier
    	print cmd8
    	p8 = Popen(cmd8, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command8out = p8.stdout.read()
    	print command8out

    	time.sleep(180)

    def test_I_delete_db_instance_rds(self):

    	cmd9 = "jcs rds delete-db-instance --db-instance-identifier "+self.newdbinstanceidentifier+" --skip-final-snapshot"
    	print cmd9
    	p9 = Popen(cmd9, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    	command9out = p9.stdout.read()
    	print command9out

    def responsecapture(ret,command):
    
    	rdsf=0
    	rdsp=0

   	if ret !=0:
        	print 'fail of command',command
        	rdsf = rdsf + 1
    	else:
        	print 'pass of command',command
        	rdsp = rdsp + 1

    	print rdsf 
    	print rdsp


     
if __name__ == '__main__':

    unittest.main()
    HTMLTestRunner.main()

