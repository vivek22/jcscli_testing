import json

def is_json(myjson):
    try:
        json_object = json.loads(myjson)
        ## json validation ##
	print "json is validated"
    except ValueError, e:
        return False
    return True

def get_upload_id(uploadjson):
        data = json.loads(uploadjson)
        upload_id=data['InitiateMultipartUploadResult']['UploadId']
	return upload_id


