#!/usr/bin/python

__author__= " VIVEK SHARMA "

import os
import sys
import re
import string
import time
import ConfigParser
import datetime
import HTMLTestRunner
import unittest
from subprocess import Popen, PIPE, STDOUT
import subprocess
import ConfigParser
from moduledssids import *

class DssTest(unittest.TestCase):

    configParser = ConfigParser.RawConfigParser()
    configFilePath = r'cliconfig'
    configParser.read(configFilePath)
    clibucket1 = configParser.get('DSS', 'clibucket1')
    clibucket2 = configParser.get('DSS', 'clibucket2')    

    def test_A_create_bucket1(self):

        ddrun1 = "dd if=/dev/zero of=myobj bs=1b count=1"
        p1 = Popen(ddrun1, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        time.sleep(10)
        print ddrun1

        command = "jcs dss create-bucket --bucket "+self.clibucket1
        print command
        p2 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p2.stdout.read()
        print commandout

    def test_B_create_bucket2(self):

        command = "jcs dss create-bucket --bucket "+self.clibucket2
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_C_list_buckets(self):

        command = "jcs dss list-buckets"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_D_put_object1(self):

        command = "jcs dss put-object --bucket "+self.clibucket1+" --key myobj --body myobj"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_E_put_object2(self):

        command = "jcs dss put-object --bucket "+self.clibucket2+" --key myobj --body myobj"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_F_copy_object(self):

        command = "jcs dss copy-object --copy-source "+self.clibucket1+"/myobj --key myobj --bucket "+self.clibucket2
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_G_get_object(self): 

        command = "jcs dss get-object --bucket "+self.clibucket1+" --key myobj"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_H_list_objects(self):

        command = "jcs dss list-objects --bucket "+self.clibucket1
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_I_head_bucket1(self): 

        command = "jcs dss head-bucket --bucket "+self.clibucket1
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_J_create_bucket2(self):

        command = "jcs dss head-object --bucket "+self.clibucket1+" --key myobj"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_K_get_presigned_url(self):

        command = "jcs dss get-presigned-url --bucket "+self.clibucket1+" --key myobj --expiry 1000"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_L_delete_object1(self):

        command = "jcs dss delete-object --bucket "+self.clibucket1+" --key myobj"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_M_delete_object2(self):

        command = "jcs dss delete-object --bucket "+self.clibucket2+" --key myobj"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_N_delete_bucket1(self):

        command = "jcs dss delete-bucket --bucket "+self.clibucket1
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout


    def test_O_create_multipart_upload_bucket2(self):	

        ddrun2 = "dd if=/dev/zero of=myobj2 bs=1k count=1"
        p1 = Popen(ddrun2, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        time.sleep(10)
        print ddrun2  
 
        command = "jcs dss create-multipart-upload --bucket "+self.clibucket2+" --key 'myobj2' | grep -v 'Request-Id:'"
        print command
        p2 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	global upload_id
        uploadjson = p2.stdout.read()
        upload_id = get_upload_id(uploadjson)
        print uploadjson
        print upload_id

    def test_P_upload_part_bucket2(self):

        command = "jcs dss upload-part --bucket "+self.clibucket2+" --key myobj2 --part-number 1 --body myobj2 --upload-id "+upload_id
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout


    def test_Q_list_parts_bucket2(self):

        command = "jcs dss list-parts --bucket "+self.clibucket2+" --key myobj2 --upload-id "+upload_id
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout


    def test_R_list_multipart_uploads_bucket2(self):

        command = "jcs dss list-multipart-uploads --bucket "+self.clibucket2
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

    def test_S_abort_multipart_upload_bucket2(self):

        command = "jcs dss abort-multipart-upload --bucket "+self.clibucket2+" --key 'myobj2' --upload-id "+upload_id
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout


    def test_T_delete_myobj2(self):

        command = "jcs dss delete-object --bucket "+self.clibucket2+" --key myobj2"
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout

        cmd2 = "rm -rf myobj2"
        p2 = Popen(cmd2, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	print cmd2
        cmd3 = "rm -rf myobj"
        p3 = Popen(cmd3, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
	print cmd3

    def test_U_delete_bucket2(self):

        command = "jcs dss delete-bucket --bucket "+self.clibucket2
        print command
        p1 = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        commandout = p1.stdout.read()
        print commandout
	
if __name__ == '__main__':
    unittest.main()
    HTMLTestRunner.main()
